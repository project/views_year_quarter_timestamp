<?php

/**
 * Implements hook_views_data().
 */
function views_year_quarter_timestamp_views_data() {
  $data['views_year_quarter_timestamp']['table']['group'] = t('Global');
  $data['views_year_quarter_timestamp']['table']['join'] = [
    // Exist in all views.
    '#global' => [],
  ];
  $data['views_year_quarter_timestamp']['field_views_year_quarter_timestamp'] = [
    'title' => t('Year-Quarter Timestamp Field'),
    'help' => t('Views field that returns the timestamp of a given year and quarter.'),
    'field' => [
      'id' => 'field_views_year_quarter_timestamp',
    ],
  ];

  $data['views']['views_year_quarter_timestamp_sort'] = [
    'title' => t('Year-Quarter Timestamp Sort'),
    'help' => t('Displays a year-quarter timestamp sort option.'),
    'sort' => [
      'id' => 'views_year_quarter_timestamp_sort',
    ],
  ];

  return $data;
}
