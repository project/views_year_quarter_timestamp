# Views Year-Quarter Timestamp

This module takes year and quarter from referenced taxonomy terms and returns a timestamp. Use
Views Year-Quarter Timestamp if you want to filter your content with quarters of a specific year.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_year_quarter_timestamp).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_year_quarter_timestamp).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the following modules:

- [Views](https://www.drupal.org/project/views)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Add a year taxonomy and taxonomy terms should be in example: '2020', '2021', '2022' ...
2. Add a quarter taxonomy and terms should be exactly: 'Q1', 'Q2', 'Q3' and 'Q4'.
3. Reference these year and quarter vocabularies in your content type.
4. Then in your view add the field 'Year-Quarter Timestamp Field'.

## Maintainers

- Daniel Cothran - [andileco](https://www.drupal.org/u/andileco)
- Mamadou Diao Diallo - [diaodiallo](https://www.drupal.org/u/diaodiallo)
