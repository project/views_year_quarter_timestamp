<?php

namespace Drupal\views_year_quarter_timestamp\Plugin\views\sort;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Plugin\views\sort\SortPluginBase;

/**
 * Provides a sort for the Views-Quarter Timestamp field.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsFilter("views_year_quarter_timestamp_sort")
 */
class YearQuarterTimestamp extends SortPluginBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function query() {}

}
