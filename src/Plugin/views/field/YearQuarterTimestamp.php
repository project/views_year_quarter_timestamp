<?php

namespace Drupal\views_year_quarter_timestamp\Plugin\views\field;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @file
 * Defines Drupal\views_year_quarter_timestamp\Plugin\views\field\YearQuarterTimestamp.
 */

/**
 * Field handler to generate a timestamp from a given year and quarter.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("field_views_year_quarter_timestamp")
 */
class YearQuarterTimestamp extends FieldPluginBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   */
  protected $entityTypeManager;

  /**
   * YearQuarter constructor.
   *
   * @param array $configuration
   *   The plugin configurations.
   * @param $plugin_id
   *   The plugin id.
   * @param $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManager $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Sets the initial field data at zero.
   */
  public function query() {
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    $options['year'] = ['default' => []];
    $options['quarter'] = ['default' => []];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $all_fields = $this->displayHandler->getFieldLabels();
    // Remove any field that have been excluded from the display from the list.
    foreach ($all_fields as $key => $field) {
      $exclude = $this->view->display_handler->handlers['field'][$key]->options['exclude'];
      if ($exclude) {
        unset($all_fields[$key]);
      }
    }

    // Offer to include only those fields that follow this one.
    $field_options = array_slice($all_fields, 0, array_search($this->options['id'], array_keys($all_fields)));
    $form['year'] = [
      '#type' => 'select',
      '#title' => $this->t('Year Provider'),
      '#description' => $this->t('Field that outputs a year value.'),
      '#options' => $field_options,
      '#default_value' => $this->options['year'],
    ];
    $form['quarter'] = [
      '#type' => 'select',
      '#title' => $this->t('Quarter Provider'),
      '#description' => $this->t('Field that outputs a quarter value.'),
      '#options' => $field_options,
      '#default_value' => $this->options['quarter'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(ResultRow $values, $field = NULL) {
    parent::getValue($values, $field);
    $entity_type_id = $this->getEntity($values)->getEntityTypeId();
    $year_field = $this->options['year'] ? $entity_type_id . '__' . $this->options['year'] . '_' . $this->options['year'] . '_target_id' : '';
    // Get the taxonomy term name for the year from the tid returned by the $year_field.
    $year_tid = !empty($values->{$year_field}) ? $values->{$year_field} : '';
    if ($year_tid) {
      $year_term = $this->entityTypeManager->getStorage('taxonomy_term')->load($year_tid);
      $year = $year_term->getName();
    }
    else {
      $year = '';
    }

    $quarter_field = $this->options['quarter'] ? $entity_type_id . '__' . $this->options['quarter'] . '_' . $this->options['quarter'] . '_target_id' : '';
    // Get the taxonomy term name for the quarter from the tid returned by the $quarter_field.
    $quarter_tid = !empty($values->{$quarter_field}) ? $values->{$quarter_field} : '';
    if ($quarter_tid) {
      $quarter_term = $this->entityTypeManager->getStorage('taxonomy_term')->load($quarter_tid);
      $quarter = $quarter_term->getName();
    }
    else {
      $quarter = '';
    }

    // If quarter is not empty, translate it from Q1 to respective two-digit month, then combine with year and day.
    if ($quarter && $year) {
      $numeric_quarter = match ($quarter) {
        'Q1' => '01',
        'Q2' => '04',
        'Q3' => '07',
        'Q4' => '10',
        default => '',
      };
      $date_string = $year . '-' . $numeric_quarter . '-01';
    }
    else {
      $date_string = '';
    }
    // If $date_string is not empty, convert it to a timestamp.
    if ($date_string) {
      $date_timestamp = $this->getDateTimestamp($date_string);
    }
    else {
      $date_timestamp = '';
    }

    return (int) $date_timestamp;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * Return a JavaScript timestamp from a given date string.
   *
   * @param string $date_str
   *
   * @return float|int
   */
  public function getDateTimestamp(string $date_str) {
    return $this->getDateObjectFromString($date_str)->getTimestamp() * 1000;
  }

  /**
   * Return a DateTime object from a given date string.
   *
   * @param string $date_str
   *
   * @return \Drupal\Component\Datetime\DateTimePlus
   */
  public function getDateObjectFromString(string $date_str) {
    if (strpos($date_str, '-')) {
      [$year, $month, $day] = explode('-', $date_str);
    }
    else {
      $year = substr($date_str, 0, 4);
      $month = substr($date_str, 4, 2);
      $day = substr($date_str, 6, 2);
    }

    return DateTimePlus::createFromArray([
      'year' => $year,
      'month' => $month,
      'day' => $day,
    ], new \DateTimeZone('UTC'));
  }

}
